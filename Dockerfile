FROM python:3.10-alpine
COPY . /usr/src/discord-coworking
WORKDIR /usr/src/discord-coworking
RUN apk add --virtual build-dependencies build-base \
    && python setup.py install \
    && apk del build-dependencies \
    && rm -rf /var/cache/apk/*
RUN rm -rf /usr/src/discord-coworking
WORKDIR /root
CMD ['ash','-c' ,'discord-coworking bot']
