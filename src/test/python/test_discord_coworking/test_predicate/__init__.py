from unittest import TestCase
from discord_coworking.command.predicate import ByAttributesPredicate


class Object:

    def __init__(self, attr):
        self.__dict__.update(attr)

    @classmethod
    def create(cls, **kwargs):
        return cls(kwargs)


class PredicateTest(TestCase):

    def test_should_be_true_when_one_attribute_matches(self):
        obj = Object.create(id=1)
        predicate = ByAttributesPredicate.create(
            id=1
        )
        self.assertTrue(predicate(obj))

    def test_should_be_true_when_all_attribute_matches(self):
        obj = Object.create(id=1, name='fuu')
        predicate = ByAttributesPredicate.create(
            id=1,
            name='fuu',
        )
        self.assertTrue(predicate(obj))

    def test_should_be_false_when_one_attribute_not_matches(self):
        obj = Object.create(id=1)
        predicate = ByAttributesPredicate.create(
            id=2,
        )
        self.assertFalse(predicate(obj))

    def test_should_be_false_when_not_all_attribute_matches(self):
        obj = Object.create(id=1, name='fuu')
        predicate = ByAttributesPredicate.create(
            id=2,
            name='fuu',
        )
        self.assertFalse(predicate(obj))


class NegPredicateTest(TestCase):

    def test_should_be_false_when_one_attribute_matches(self):
        obj = Object.create(id=1)
        predicate = -ByAttributesPredicate.create(
            id=1
        )
        self.assertFalse(predicate(obj))

    def test_should_be_true_when_one_attribute_not_matches(self):
        obj = Object.create(id=1)
        predicate = -ByAttributesPredicate.create(
            id=2,
        )
        self.assertTrue(predicate(obj))


class OrPredicateTest(TestCase):
    def test_should_be_true_when_one_or_another_matches(self):
        obj = Object.create(id=1, name='fuu')
        predicate = ByAttributesPredicate.create(
            id=1
        ) | ByAttributesPredicate.create(
            name='foo'
        )
        self.assertTrue(predicate(obj))

    def test_should_be_false_when_one_or_another_not_matches(self):
        obj = Object.create(id=1, name='fuu')
        predicate = ByAttributesPredicate.create(
            id=2
        ) | ByAttributesPredicate.create(
            name='foo'
        )
        self.assertFalse(predicate(obj))


class AndPredicateTest(TestCase):
    def test_should_be_true_when_one_and_another_matches(self):
        obj = Object.create(id=1, name='fuu')
        predicate = ByAttributesPredicate.create(
            id=1
        ) & ByAttributesPredicate.create(
            name='fuu'
        )
        self.assertTrue(predicate(obj))

    def test_should_be_false_when_one_and_another_not_matches(self):
        obj = Object.create(id=1, name='fuu')
        predicate = ByAttributesPredicate.create(
            id=1
        ) & ByAttributesPredicate.create(
            name='foo'
        )
        self.assertFalse(predicate(obj))
